* Añadida la opción de sistema numeral
* Añadida la opción de rango de valores
*Añadida la opción de regla 3b
* Actualización de niveles
* Mejora de la legibilidad en pantallas pequeñas
* Mejoras mínimas en la interfaz
* Corrección de errores
