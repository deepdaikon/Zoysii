* Aggiunta opzione per il sistema numerico
* Aggiunta opzione per gamma di valori
* Aggiunta opzione regola 3b
* Aggiornati livelli
* Migliorata leggibilità su schermi piccoli
* Miglioramenti minori all'interfaccia
* Risolti bug
