* Aggiunti nuovi livelli
* Aggiunto supporto a partite contro 3 giocatori (CPU)
* Nuova interfaccia
* Nuova icona
* Miglioramenti delle prestazioni
* Risolti bug
