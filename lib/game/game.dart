import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zoysii/game/utils/board.dart';
import 'package:zoysii/game/utils/direction.dart';
import 'package:zoysii/game/utils/level.dart';
import 'package:zoysii/game/utils/player.dart';
import 'package:zoysii/game/utils/random.dart';
import 'package:zoysii/game/utils/result.dart';
import 'package:zoysii/ui/screens/game/game_page.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';
import 'package:zoysii/utils/numeral_system.dart';

/// Current game
Game? game;

/// Game modes available
enum GameMode { single, multi, level }

class Game {
  Game(
    this._gamePage,
    this.mode,
    this.players, {
    this.speed = 0,
    this.board,
    int? inputId,
    List<int>? level,
  }) : humanPlayer = players.firstWhere((p) => p.human) {
    humanPlayer = players.firstWhere((p) => p.human);
    loopRepeat = inputId != null;
    selectedNumeralSystem =
        isLevelMode ? NumeralSystem.Arabic : settings.numeralSystem;
    start(newMatchId: inputId, levelId: level);
  }

  /// Game mode
  final GameMode mode;

  /// Game page state
  final GamePageState _gamePage;

  /// Milliseconds between players moves. 0 if the game is turn based
  final int? speed;

  /// Game board
  Board? board;

  bool get isLevelMode => mode == GameMode.level;
  bool get isSingleMode => mode == GameMode.single;
  bool get isMultiMode => mode == GameMode.multi;

  /// Match id
  /// In levels mode id equals to -(selectedLevel[0] * 7 + selectedLevel[1] + 1)
  int? _id;
  int? get id => _id;

  /// Match status
  bool _locked = false;
  bool _pause = true;
  bool get isPaused => _pause;
  set pause(bool newValue) {
    _pause = newValue;
    if (_pause == false && speed == 0 && !_activePlayer.human) {
      movePlayer(_activePlayer);
    }
  }

  /// Starting match date, saved as milliseconds since epoch
  late int initDate;

  /// List of players
  late List<Player> players;

  /// List of player sorted by points, moves and status
  List<Player> get ranking => List.from(players).cast<Player>()
    ..sort((a, b) {
      if (a.alive != b.alive) return a.alive ? -1 : 1;
      if (a.points != b.points) return b.points.compareTo(a.points);
      return a.moves.compareTo(b.moves);
    });

  /// List of active players
  List<Player> get alivePlayers =>
      players.where((player) => player.alive).toList();
  bool get withFriend => players.where((player) => player.human).length > 1;

  /// Players
  Player get playerOne => players.first;
  Player? get playerTwo => players.length > 1 ? players[1] : null;
  Player? get playerThree => players.length > 2 ? players[2] : null;
  Player? get playerFour => players.length > 3 ? players[3] : null;
  Player humanPlayer;

  /// Index in alivePlayers of the player that can move (in turn based game)
  int _playerIndexTurn = 0;
  Player get _activePlayer => alivePlayers[_playerIndexTurn];

  /// True if the screen should be upside down
  bool get rotatedBox =>
      settings.rotatedDisplay &&
      _activePlayer.human &&
      players.where((p) => p.human).toList().indexOf(_activePlayer) % 2 != 0;

  /// Best result for this grid size. Used only for single player match
  late int _bestResult;

  // Result to beat
  int get resultToBeat {
    if (isMultiMode) {
      var rankingCopy = List.from(ranking).cast<Player>();
      if (rankingCopy.first.human && rankingCopy.length > 1) {
        return rankingCopy.first.points - rankingCopy[1].points;
      } else {
        return rankingCopy.firstWhere((p) => p.human).points -
            rankingCopy.first.points;
      }
    } else {
      return _bestResult;
    }
  }

  /// Numeral system used during the match
  late NumeralSystem selectedNumeralSystem;

  /// Selected level (null if it is not level mode)
  Level? level;

  /// True if the match should be repeated again and again
  /// Used for matches loaded from Ranking Page
  /// It affects game dialogs
  late bool loopRepeat;

  /// Counts how many times restart button was pressed
  /// Used only in levels mode
  int restartCount = 0;

  /// Game timer used to move CPU players in multi-player mode
  Timer? timer;

  /// Return true if player can move
  bool canMove(Player player) =>
      !isPaused && !_locked && (speed != 0 || player == _activePlayer);

  /// Manage input
  void inputController(Direction direction) {
    if (speed != 0 || _activePlayer.human) {
      movePlayer(speed != 0 ? humanPlayer : _activePlayer,
          rotatedBox ? direction.opposite : direction);
    }
  }

  /// Start a new match
  void start({int? newMatchId, List<int>? levelId}) {
    for (final player in players) {
      player.clear();
    }
    initDate = DateTime.now().millisecondsSinceEpoch;
    if (isLevelMode) {
      level = levels
          .firstWhere((l) => listEquals(l.levelId, levelId ?? level!.levelId));
      playerOne.position = level!.startingPosition;
      board = Board.fromLevel(level!);
    } else {
      timer?.cancel();
      _id = CurrentRandom.set(newMatchId ?? id);
      board = Board();
      for (final player in players) {
        player.position = _getStartingPosition(players.indexOf(player));
      }
      if (isMultiMode) {
        _playerIndexTurn = 0;
        if (speed! > 0) {
          timer = Timer.periodic(Duration(milliseconds: speed!), (_) {
            for (final player in alivePlayers) {
              if (!player.human) movePlayer(player);
            }
          });
        }
      } else {
        var resultsCopy = List.from(results.where((result) =>
            result.matchId >= 0 && result.sideLength == board!.width));
        resultsCopy.sort((a, b) => b.points - a.points);
        _bestResult = resultsCopy.isNotEmpty
            ? resultsCopy.first.points
            : board!.width * 24;
      }
    }
    if (isMultiMode && speed == 0 && !_activePlayer.human) {
      Future.delayed(const Duration(milliseconds: 1000), () => pause = false);
    } else {
      pause = false;
    }
  }

  /// Load next level or a new random match
  void nextMatch() {
    List<int>? levelCopy;
    if (!isLevelMode) {
      _id = -1;
    } else {
      levelCopy = List.from(level!.levelId);
      restartCount = 0;
      if (levelCopy[1] < 6) {
        ++levelCopy[1];
      } else if (levelCopy[0] < layers.length - 1) {
        ++levelCopy[0];
        levelCopy[1] = 0;
      }
    }
    start(levelId: levelCopy);
  }

  /// Get starting position for each player in multiplayer mode
  int _getStartingPosition(int playerIndex) {
    switch (playerIndex) {
      case 1:
        return board!.grid.length - 1;
      case 2:
        return board!.width - 1;
      case 3:
        return board!.grid.length - board!.width;
      default:
        return 0;
    }
  }

  /// Move player and update tiles value
  void movePlayer(Player player, [Direction? direction]) {
    if (!canMove(player)) return;

    // Position increase
    int delta;

    // Update tiles until condition == true (ie until the end of the board)
    Function condition;

    var newDirection = direction ??
        ExtDirection.random(player.position, ranking.first.position, board!);
    switch (newDirection) {
      case Direction.right:
        delta = 1;
        condition = (index) => index % board!.width != 0;
        break;
      case Direction.left:
        delta = -1;
        condition = (index) => (index + 1) % board!.width != 0;
        break;
      case Direction.up:
        delta = -board!.width;
        condition = (index) => index >= 0;
        break;
      case Direction.down:
        delta = board!.width;
        condition = (index) => index < board!.grid.length;
        break;
    }

    var startingPosition = player.position;
    var startingTileValue = board!.grid[player.position];

    // Ignore this move if player collided the border
    if (!condition(player.position + delta) ||
        board!.grid[player.position + delta] == null) {
      if (direction == null) movePlayer(player, newDirection.opposite);
      return;
    } else {
      player.position += delta;
      player.moves++;
      // Update tiles
      if (board!.grid[startingPosition] != 0) {
        for (var tile = player.position; condition(tile); tile += delta) {
          if (board!.grid[tile] == 0 || board!.grid[tile] == null) continue;
          var previousValue = board!.grid[tile];
          board!.grid[tile] = (board!.grid[tile]! - startingTileValue!).abs();
          if (board!.grid[tile] == 0) {
            _updatePoints(tile, player, startingTileValue);
            board!.grid[startingPosition] = 0;
          } else if (board!.grid[tile]! <=
              (isLevelMode ? 2 : board!.biggestLow!)) {
            board!.grid[tile] = previousValue! + startingTileValue;
          }
        }
      }
    }
    _someoneMoved();
  }

  /// Update player points
  void _updatePoints(int tile, Player player, int deletedTileValue) {
    player.points += 2 * deletedTileValue;
    for (final p in alivePlayers) {
      if (p != player && p.position == tile && p.moves != 0) {
        p.alive = false;
        player.kills++;
        player.points += 10;
        _playerIndexTurn = alivePlayers.indexOf(player);
      }
    }
  }

  /// Update game page state and check match status
  void _someoneMoved() {
    _gamePage.update();
    if (_end()) return;
    if (speed == 0) {
      _locked = true;
      Future.delayed(
          Duration(milliseconds: settings.rotatedDisplay ? 1000 : 300), () {
        if (++_playerIndexTurn >= alivePlayers.length) _playerIndexTurn = 0;
        _gamePage.update();
        _locked = false;
        if (!_activePlayer.human) {
          Future.delayed(const Duration(milliseconds: 500),
              () => movePlayer(_activePlayer));
        }
      });
    }
  }

  /// Check if someone won the match
  bool _end() {
    bool win = false;
    String? title;
    String? content;
    Color? titleColor;
    if (isMultiMode && players.every((p) => !p.human || !p.alive)) {
      win = false;
      title = game!.withFriend ? 'You all lost'.i18n : 'You lost'.i18n;
      content = game!.withFriend
          ? 'You have all been deleted by CPU.'.i18n
          : 'You have been deleted.'.i18n;
      titleColor = ranking.first.color;
    } else if (isMultiMode && alivePlayers.length == 1) {
      win = true;
      title = game!.withFriend
          ? '%s won'.i18n.fill([alivePlayers.single.name])
          : 'You won'.i18n;
      content = game!.withFriend
          ? '%s won because it deleted all its opponents.'
              .i18n
              .fill([alivePlayers.single.name])
          : 'You won because you deleted your opponents.'.i18n;
      titleColor = alivePlayers.single.color;
    } else if (board!.remainingTiles == 0) {
      switch (mode) {
        case GameMode.level:
          if (playerOne.moves <= level!.maxMoves) {
            win = true;
            _saveData();
            saveNextLevel(level!.levelId);
            title = 'Level solved'.i18n;
            content = 'You solved the level by doing %s moves.'
                .i18n
                .fill([game!.playerOne.moves]);
            if (game!.playerOne.moves > game!.level!.minMoves) {
              content += '\n' +
                  "You could have solved it in fewer moves, but it's ok.".i18n;
            } else if (game!.playerOne.moves < game!.level!.maxMoves) {
              content += '\n' +
                  'It took you fewer moves than you had available, good job!'
                      .i18n;
            }
            if (game!.level!.levelId[1] == 6 &&
                game!.level!.levelId[0] + 1 == nextLevel[0] &&
                nextLevel[1] == 0) {
              content += '\n\n' +
                  'Congratulations! You unlocked the %s numeral system. It is now available outside levels mode.'
                      .i18n
                      .fill([NumeralSystem.values[nextLevel[0]].name.i18n]);
            }
            titleColor = Colors.green;
          } else {
            win = false;
            title = 'Too many moves'.i18n;
            content =
                'You made %s moves. You failed to solve the level in %s moves.'
                    .i18n
                    .fill([game!.playerOne.moves, game!.level!.maxMoves]);
            titleColor = Colors.red;
          }
          break;
        case GameMode.single:
          win = true;
          _saveData();
          title = 'Congratulations'.i18n;
          content = 'You scored %s points in %s moves'
              .i18n
              .fill([game!.playerOne.points, game!.playerOne.moves]);
          break;
        case GameMode.multi:
          if (ranking.first.human) {
            win = true;
            title = game!.withFriend
                ? '%s won'.i18n.fill([ranking.first.name])
                : 'You won'.i18n;
            content = game!.withFriend
                ? '%s won by scoring more points than all its opponents.'
                    .i18n
                    .fill([ranking.first.name])
                : 'You won by scoring more points than your opponents.'.i18n;
            titleColor = ranking.first.color;
          } else {
            win = false;
            title = game!.withFriend ? 'You all lost'.i18n : 'You lost'.i18n;
            content = game!.withFriend
                ? 'Your CPU opponent won by scoring the most points.'.i18n
                : 'Your opponent won by scoring more points than you.'.i18n;
            titleColor = ranking.first.color;
          }
          break;
      }
    }
    if (title != null && content != null) {
      endDialog(_gamePage,
          win: win, title: title, content: content, titleColor: titleColor);
      return true;
    }
    return false;
  }

  /// Save match result
  void _saveData() {
    saveResult(Result(
        startDate: initDate,
        endDate: DateTime.now().millisecondsSinceEpoch,
        matchId: id ?? -(level!.levelId[0] * 7 + (level!.levelId[1] + 1)),
        sideLength: board!.width,
        moves: playerOne.moves,
        points: playerOne.points,
        minBoardInt: board!.minValue,
        maxBoardInt: board!.maxValue,
        biggestLow: board!.biggestLow));
  }

  /// Dispose the game
  void dispose() {
    timer?.cancel();
  }
}
