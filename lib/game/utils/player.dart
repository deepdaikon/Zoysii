import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';

class Player {
  Player({required this.color, this.human = true});
  Player.single() : this(color: playerColor.first);

  /// Player position
  int position = 0;

  /// Player color
  final Color color;
  Color get displayColor => alive ? color : Colors.grey.shade500;

  /// Player name
  String get name => color.name + (human ? '' : ' (CPU)');

  /// True if this is controlled by a human
  bool human;

  /// Moves count
  int moves = 0;

  /// Earned points
  int points = 0;

  /// Player status
  bool alive = true;

  /// Number of deleted players
  int kills = 0;

  /// Reset player
  void clear() {
    alive = true;
    points = 0;
    moves = 0;
    kills = 0;
  }
}
