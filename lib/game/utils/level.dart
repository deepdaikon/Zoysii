import 'dart:math';

import 'package:zoysii/game/utils/direction.dart';

/// Game level
class Level {
  Level(Map<String, dynamic> json)
      : name = json['name'],
        levelId = json['level'].cast<int>(),
        maxMoves = json['max_moves'],
        minMoves = json['min_moves'] ?? json['max_moves'],
        solution =
            List.from((json['solution'] ?? []).map((v) => Direction.values[v])),
        _hintLength = json['hintLength'] ?? 2,
        startingPosition = json['starting_position'] ?? 0,
        grid = json['grid'].cast<int?>() {
    width = json['width'] ?? sqrt(grid.length).toInt();
  }

  /// Level name
  final String name;

  /// levelId[0] == layer, levelId[1] == level
  final List<int> levelId;

  /// Moves allowed in this level
  final int maxMoves;

  /// Minimum number of moves
  final int minMoves;

  /// Solution
  final List<Direction> solution;
  final int _hintLength;
  List<Direction> get hint => solution.sublist(0, _hintLength);

  /// Game grid
  final List<int?> grid;

  /// Board width
  late final int width;

  /// Player starting position
  final int startingPosition;
}
