import 'package:flutter/services.dart';
import 'package:zoysii/game/utils/board.dart';
import 'package:zoysii/game/utils/random.dart';

// i18n: 'down'.i18n, 'up'.i18n, 'right'.i18n, 'left'.i18n
enum Direction { down, up, right, left }

extension ExtDirection on Direction {
  static Direction? fromKeyboardKey(PhysicalKeyboardKey key) {
    if (key == PhysicalKeyboardKey.arrowDown) return Direction.down;
    if (key == PhysicalKeyboardKey.arrowUp) return Direction.up;
    if (key == PhysicalKeyboardKey.arrowRight) return Direction.right;
    if (key == PhysicalKeyboardKey.arrowLeft) return Direction.left;
    return null;
  }

  Direction get opposite {
    switch (this) {
      case Direction.up:
        return Direction.down;
      case Direction.down:
        return Direction.up;
      case Direction.right:
        return Direction.left;
      case Direction.left:
        return Direction.right;
    }
  }

  /// Get a random direction based on board status and player position
  static Direction random(int position, int targetPosition, Board board) {
    Direction? direction;
    for (var j = position; j % board.width != 0; j++) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.right;
      }
    }
    for (var j = position; (j + 1) % board.width != 0; j--) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.left;
      }
    }
    for (var j = position; j < board.grid.length; j += board.width) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.down;
      }
    }
    for (var j = position; j >= 0; j -= board.width) {
      if (board.grid[j] == board.grid[position] &&
          j != position &&
          board.grid[position] != 0) {
        direction = Direction.up;
      }
    }
    if (direction == null) {
      if (targetPosition < position) {
        direction =
            CurrentRandom.get(0, 1) == 1 ? Direction.up : Direction.left;
      }
      if (targetPosition > position) {
        direction =
            CurrentRandom.get(0, 1) == 1 ? Direction.right : Direction.down;
      }
      if (targetPosition == position) {
        if (position < (board.grid.length / 2).round()) {
          direction =
              CurrentRandom.get(0, 1) == 1 ? Direction.right : Direction.down;
        } else {
          direction =
              CurrentRandom.get(0, 1) == 1 ? Direction.up : Direction.left;
        }
      }
    }
    return direction!;
  }
}
