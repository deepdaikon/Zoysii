/// Match result
class Result {
  Result({
    required this.matchId,
    required this.startDate,
    required this.endDate,
    required this.sideLength,
    required this.moves,
    required this.points,
    required this.minBoardInt,
    required this.maxBoardInt,
    required this.biggestLow,
  });

  /// Import result from a Map<String, dynamic>
  Result.fromJson(Map<String, dynamic> json)
      : matchId = json['matchId'] ?? 0,
        startDate = json['startDate'] ?? 0,
        endDate = json['endDate'] ?? 0,
        sideLength = json['sideLength'] ?? 0,
        moves = json['moves'] ?? 0,
        points = json['points'] ?? 0,
        minBoardInt = json['minBoardInt'] ?? 0,
        maxBoardInt = json['maxBoardInt'] ?? 0,
        biggestLow = json['biggestLow'] ?? 0;

  /// Export result as a Map<String, dynamic>
  Map<String, dynamic> toJson() => {
        'matchId': matchId,
        'startDate': startDate,
        'endDate': endDate,
        'sideLength': sideLength,
        'moves': moves,
        'points': points,
        'minBoardInt': minBoardInt,
        'maxBoardInt': maxBoardInt,
        'biggestLow': biggestLow,
      };

  /// Match id
  /// In levels mode it equals to: -(world * 7 + level + 1)
  final int matchId;

  /// Moves done by player one
  final int moves;

  /// Points scored by player one
  final int points;

  /// Match grid size length
  final int sideLength;

  /// Board range of values
  final int? minBoardInt;
  final int? maxBoardInt;

  /// Biggest "low number" for rule 3b
  final int? biggestLow;

  /// Match start date, saved as milliseconds since epoch
  final int startDate;

  /// Match end date, saved as milliseconds since epoch
  final int endDate;

  /// Match duration
  Duration get _duration => Duration(milliseconds: endDate - startDate);
  String get formattedDuration =>
      _duration.inMinutes.remainder(60).toString().padLeft(2, '0') +
      ':' +
      _duration.inSeconds.remainder(60).toString().padLeft(2, '0');
}
