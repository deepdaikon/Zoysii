import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/rules/utils/rule.dart';

class RulePage extends StatelessWidget {
  RulePage(this.rule);
  final Rule rule;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Center(
            child: Container(
              width: 275,
              height: 275,
              decoration: rule.image != null
                  ? BoxDecoration(
                      border: rule.border
                          ? Border.all(width: 0.4, color: Colors.black87)
                          : null,
                      borderRadius: borderRadius,
                      image: DecorationImage(
                        image: AssetImage(
                          Theme.of(context).brightness == Brightness.dark
                              ? rule.lightImage ?? rule.image!
                              : rule.image!,
                        ),
                      ),
                    )
                  : null,
              child:
                  rule.image != null ? null : const Icon(Icons.done, size: 180),
            ),
          ),
        ),
        Expanded(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: Text(
              rule.title,
              style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Center(
            child: SingleChildScrollView(
              primary: false,
              child: Container(
                margin: const EdgeInsets.all(20),
                child: Text(
                  rule.subtitle,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 18, height: 1.4),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
