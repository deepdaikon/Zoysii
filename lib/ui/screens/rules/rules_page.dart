import 'package:flutter/material.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/rules/resources/rules.dart';
import 'package:zoysii/ui/screens/rules/widgets/rule_page.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

class RulesPage extends StatefulWidget {
  RulesPage([this.homePage]);
  final StatelessWidget? homePage;

  @override
  State<RulesPage> createState() => _RulesPageState();
}

class _RulesPageState extends State<RulesPage>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    _controller = TabController(vsync: this, length: rules().length);
    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: settings.firstRun
            ? Container()
            : IconButton(
                icon: Icon(Icons.close,
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.white70
                        : Colors.black87,
                    size: 32),
                onPressed: () {
                  if (widget.homePage == null) {
                    Navigator.pop(context);
                  } else {
                    onExit();
                    Navigator.pushReplacement(
                        context, FadeRoute(widget.homePage!));
                  }
                },
              ),
      ),
      body: TabBarView(
          controller: _controller,
          children: [for (var rule in rules()) RulePage(rule)]),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              TextButton(
                onPressed: (_controller.index > 0)
                    ? () {
                        _controller.index -= (_controller.index > 0) ? 1 : 0;
                      }
                    : null,
                child: Row(
                  children: <Widget>[
                    const Icon(Icons.navigate_before),
                    Text('Back'.i18n),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      for (var i = 1; i < _controller.length - 1; i++)
                        CircleAvatar(
                          radius: _controller.index == i ? 4 : 3,
                          backgroundColor: _controller.index == i
                              ? Theme.of(context).colorScheme.onBackground
                              : Colors.grey,
                        ),
                    ],
                  ),
                ),
              ),
              if (_controller.index >= _controller.length - 1)
                TextButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).toggleableActiveColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)))),
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Ok'.i18n.toUpperCase(),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  onPressed: () {
                    if (widget.homePage == null) {
                      Navigator.pop(context);
                    } else {
                      onExit();
                      Navigator.pushReplacement(
                          context, FadeRoute(widget.homePage!));
                    }
                  },
                )
              else
                TextButton(
                  child: Row(
                    children: <Widget>[
                      Text('Next'.i18n),
                      const Icon(Icons.navigate_next),
                    ],
                  ),
                  onPressed: () {
                    if (_controller.index < _controller.length - 1) {
                      ++_controller.index;
                    }
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }

  void onExit() {
    if (settings.firstRun) {
      settings.firstRun = false;
      saveSettings();
    }
  }
}
