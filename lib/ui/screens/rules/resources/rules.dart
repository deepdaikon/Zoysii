import 'package:zoysii/ui/screens/rules/utils/rule.dart';
import 'package:zoysii/utils/i18n.dart';

/// Zoysii rules
List<Rule> rules() => [
      Rule(
        title: 'Z O Y S I I',
        subtitle:
            'Zoysii is a simple logic game.\nSwipe to the left to learn how to play.'
                .i18n,
        image: 'assets/graphics/rule_0.png',
        lightImage: 'assets/graphics/rule_0_white.png',
        border: false,
      ),
      Rule(
        title: 'You'.i18n,
        subtitle:
            'You are the red tile on a square board.\nIn the game you can move yourself by swiping horizontally or vertically.'
                .i18n,
        image: 'assets/graphics/rule_1.png',
      ),
      Rule(
        title: 'Moves'.i18n,
        subtitle:
            'When you move, your starting tile value is subtracted from each of the tiles in the direction you are going.'
                .i18n,
        image: 'assets/graphics/rule_2.gif',
      ),
      Rule(
        title: 'Low numbers'.i18n,
        subtitle:
            'Negative numbers become positive.\n\nMoreover, if the value of a tile would be equal to 1 or 2 there will be an increase instead of a decrease. (4 - 5 -> 9)'
                .i18n,
        image: 'assets/graphics/rule_3.gif',
      ),
      Rule(
        title: 'Points'.i18n,
        subtitle:
            'If the value of a tile becomes equal to zero, starting tile value becomes zero too.\n\nYou earn as many points as the value of the deleted tiles, with bonus points if you delete more than two tiles at once.'
                .i18n,
        image: 'assets/graphics/rule_4.gif',
      ),
      Rule(
        title: 'How to win'.i18n,
        subtitle:
            "The aim is to delete almost every tile while trying to make the most points.\n\nIn multiplayer matches a player can also win by deleting opponent's tile."
                .i18n,
        image: 'assets/graphics/rule_5.gif',
      ),
      Rule(
        title: "That's all".i18n,
        subtitle:
            'The best way to learn is by playing!\n\nLevels mode is a good place to start.'
                .i18n,
        border: false,
      ),
    ];
