import 'package:flutter/material.dart';
import 'package:zoysii/main.dart';
import 'package:zoysii/ui/screens/settings/widgets/option_list.dart';
import 'package:zoysii/ui/screens/settings/widgets/restore_dialog.dart';
import 'package:zoysii/utils/i18n.dart';

class SettingsPage extends StatefulWidget {
  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Settings'.i18n.toUpperCase()),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.settings_backup_restore),
              tooltip: 'Restore'.i18n,
              onPressed: () => restoreSettingsDialog(this),
            ),
          ],
        ),
        body: OptionList(this),
      );

  /// Refresh page state
  void refresh() => setState(() {});

  /// Refresh app state
  void fullRefresh() {
    refresh();
    Zoysii.of(context).setState(() {});
  }
}
