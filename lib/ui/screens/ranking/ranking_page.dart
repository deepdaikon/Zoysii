import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zoysii/game/utils/result.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/ui/screens/ranking/widgets/bottom_filter_bar.dart';
import 'package:zoysii/ui/screens/ranking/widgets/result_tile.dart';
import 'package:zoysii/utils/i18n.dart';
import 'package:zoysii/utils/local_data_controller.dart';

// i18n: 'Points'.i18n, 'Moves'.i18n, 'Date'.i18n
// ignore: constant_identifier_names
enum SortOptions { Points, Moves, Date }

/// List of match results (without levels)
late List<Result> matchResults;

/// List of results displayed in ranking page
List<Result> resultsDisplayed = [];

/// Filter: only results that have size == filter are displayed
int filter = matchResults.isNotEmpty ? matchResults.last.sideLength : 0;

/// Sort by
SortOptions sorter = SortOptions.Points;

class RankingPage extends StatefulWidget {
  @override
  RankingPageState createState() => RankingPageState();
}

class RankingPageState extends State<RankingPage> {
  @override
  Widget build(BuildContext context) {
    matchResults = List.from(results)
        .where((result) => result.matchId >= 0)
        .toList()
        .cast<Result>();
    filterBy(filter);
    sortBy(sorter);

    return Scaffold(
      appBar:
          AppBar(title: Text('Ranking'.i18n.toUpperCase()), centerTitle: true),
      body: resultsDisplayed.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(Icons.sentiment_neutral, size: 36),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Text(
                      'Nothing to show.\nYou have to play at least one match in single player mode.'
                          .i18n,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            )
          : ListView.builder(
              itemCount: resultsDisplayed.length,
              itemBuilder: (context, index) {
                var showRange = false;
                var showBigLow = false;
                for (final r in resultsDisplayed) {
                  if (!showBigLow &&
                      r.biggestLow != resultsDisplayed.first.biggestLow) {
                    showBigLow = true;
                  }
                  if (!showRange &&
                      !listEquals([
                        r.minBoardInt,
                        r.maxBoardInt
                      ], [
                        resultsDisplayed.first.minBoardInt,
                        resultsDisplayed.first.maxBoardInt
                      ])) {
                    showRange = true;
                  }
                }
                return ResultTile(index, resultsDisplayed[index],
                    showBigLow: showBigLow,
                    showRange: showRange,
                    callback: () => setState(() {}));
              }),
      bottomNavigationBar:
          resultsDisplayed.isEmpty ? null : BottomFilterBar(this),
      floatingActionButton: resultsDisplayed.isEmpty
          ? PlayButton(callback: () => setState(() {}))
          : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  /// Sort ranking list
  void sortBy(SortOptions sortOption) {
    resultsDisplayed.sort((a, b) {
      switch (sortOption) {
        case SortOptions.Points:
          return b.points - a.points;
        case SortOptions.Moves:
          return a.moves - b.moves;
        case SortOptions.Date:
          return b.startDate - a.startDate;
        default:
          return 0;
      }
    });
  }

  /// Filter result list by size
  void filterBy(int size) {
    resultsDisplayed = size == 0
        ? List.from(matchResults)
        : matchResults.where((result) => result.sideLength == size).toList();
  }

  /// Refresh page state
  void refresh() => setState(() {});
}
