import 'dart:math';

import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/ui/basic.dart';
import 'package:zoysii/utils/i18n.dart';

/// Calculate percentage values and return _PercentIndicator widget
Widget percentIndicator() {
  String? text;
  List<int> values;
  switch (game!.mode) {
    case GameMode.level:
      text = 'Remaining moves: %s'
          .i18n
          .fill([max(0, game!.level!.maxMoves - game!.playerOne.moves)]);
      values = [
        game!.level!.maxMoves - game!.playerOne.moves,
        game!.playerOne.moves
      ];
      break;
    case GameMode.single:
      text = 'Points: %s'.i18n.fill([game!.playerOne.points]) +
          (game!.isMultiMode ? ' - ${game!.playerTwo!.points}' : '');
      values = [
        max(1, game!.playerOne.points),
        game!.resultToBeat - game!.playerOne.points
      ];
      break;
    case GameMode.multi:
      values = [
        max(1, game!.playerOne.points),
        max(1, game!.playerTwo!.points),
        if (game!.players.length >= 3) max(1, game!.playerThree!.points),
        if (game!.players.length >= 4) max(1, game!.playerFour!.points),
      ];
      break;
  }
  return _PercentIndicator(text: text, values: values, colors: [
    game!.playerOne.displayColor,
    game!.playerTwo?.displayColor,
    game!.playerThree?.displayColor,
    game!.playerFour?.displayColor,
  ]);
}

/// Percent indicator widget
class _PercentIndicator extends StatelessWidget {
  _PercentIndicator({required this.values, required this.colors, this.text})
      : assert(values.length > 1 && colors.length > 1);

  /// Percentage values
  final List<int> values;

  /// Bar colors
  final List<Color?> colors;

  /// Text displayed on the percent bar
  final String? text;

  final double height = 25;

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Row(
        children: <Widget>[
          for (var i = 0; i < values.length; i++)
            Expanded(
              flex: values[i],
              child: Container(
                height: height,
                decoration: BoxDecoration(
                  color: colors.length > i && colors[i] != null
                      ? colors[i]
                      : playerColor[1],
                  border: game!.players.length > i &&
                          game!.isMultiMode &&
                          game!.speed == 0 &&
                          game!.canMove(game!.players[i])
                      ? Border(
                          bottom: BorderSide(
                              color: Theme.of(context).colorScheme.secondary,
                              width: 3))
                      : null,
                ),
                child: game!.players.length > i &&
                        game!.isMultiMode &&
                        game!.ranking.first == game!.players[i]
                    ? const Icon(Icons.star, color: Colors.white)
                    : null,
              ),
            ),
        ],
      ),
      SizedBox(
        height: height,
        child: Center(
          child: Text(
            text ?? '',
            maxLines: 1,
            style: const TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    ]);
  }
}
