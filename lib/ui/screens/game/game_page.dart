import 'package:flutter/material.dart';
import 'package:zoysii/game/game.dart';
import 'package:zoysii/game/utils/board.dart';
import 'package:zoysii/game/utils/direction.dart';
import 'package:zoysii/game/utils/player.dart';
import 'package:zoysii/ui/screens/game/widgets/dialogs.dart';
import 'package:zoysii/ui/screens/game/widgets/game_app_bar.dart';
import 'package:zoysii/ui/screens/game/widgets/game_grid.dart';
import 'package:zoysii/ui/screens/game/widgets/gamepad.dart';
import 'package:zoysii/ui/screens/game/widgets/gesture_controller.dart';
import 'package:zoysii/utils/local_data_controller.dart';

class GamePage extends StatefulWidget {
  GamePage(
    this._mode, {
    List<Player>? players,
    this.matchToLoad,
    this.selectedLevel,
    this.gameSpeed,
    this.board,
  }) : players = players ?? [Player.single()];

  /// Game mode
  final GameMode _mode;

  /// Total number of players
  final List<Player> players;

  /// Match to load
  /// Null if it should load a random match
  final int? matchToLoad;

  /// Level to load
  /// Null if !match.isLevelMode
  final List<int>? selectedLevel;

  /// Game settings
  /// Null if it should use default values
  final int? gameSpeed;

  /// Game board settings
  final Board? board;

  @override
  GamePageState createState() => GamePageState();
}

class GamePageState extends State<GamePage> with GestureController {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    game = Game(
      this,
      widget._mode,
      widget.players,
      inputId: widget.matchToLoad,
      level: widget.selectedLevel,
      speed: widget.gameSpeed,
      board: widget.board,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        child: RawKeyboardListener(
          autofocus: true,
          focusNode: FocusNode(
            onKey: (node, event) {
              if (!event.logicalKey.keyLabel.contains('Arrow')) {
                return KeyEventResult.ignored;
              }
              final direction = ExtDirection.fromKeyboardKey(event.physicalKey);
              if (direction != null) game?.inputController(direction);
              return KeyEventResult.handled;
            },
          ),
          child: RotatedBox(
            quarterTurns: game!.rotatedBox ? 2 : 0,
            child: Scaffold(
              key: scaffoldKey,
              body: Column(
                mainAxisAlignment: settings.bottomBar
                    ? MainAxisAlignment.spaceBetween
                    : MainAxisAlignment.start,
                children: [
                  if (!settings.bottomBar) GameAppBar(this),
                  Stack(children: <Widget>[
                    Container(
                        padding: game!.rotatedBox != settings.bottomBar
                            ? EdgeInsets.only(
                                top: game!.rotatedBox
                                    ? 0
                                    : MediaQuery.of(context).padding.top,
                                bottom: game!.rotatedBox
                                    ? MediaQuery.of(context).padding.top
                                    : 0)
                            : EdgeInsets.zero,
                        child: settings.inputMethod == 0
                            ? gestureController(GameGrid())
                            : GameGrid()),
                    if (settings.inputMethod == 1)
                      FloatingGamepad(game!.inputController),
                  ]),
                  if (settings.bottomBar) GameAppBar(this, bottom: true),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () {
          exitDialog(this);
          return Future.value(false);
        },
      );

  /// Update game page state
  void update() {
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    game!.dispose();
    game = null;
    super.dispose();
  }
}
