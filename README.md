# Z O Y S I I

A simple logic game: find the best path to delete every number in a square space.

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="70">](https://play.google.com/store/apps/details?id=xyz.deepdaikon.z0ysii)
[<img src="https://static.itch.io/images/badge.svg"
     alt="Get it on itch.io"
     height="45">](https://deepdaikon.itch.io/zoysii)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="70">](https://f-droid.org/packages/xyz.deepdaikon.zoysii/)

## About

[![Liberapay](https://img.shields.io/liberapay/patrons/deepdaikon.svg?logo=liberapay)](https://liberapay.com/deepdaikon/donate)
[<img src="https://www.ko-fi.com/img/githubbutton_sm.svg" alt="Ko-fi" height="20">](https://ko-fi.com/deepdaikon)
[<img src="https://translate.deepdaikon.xyz/widgets/zoysii/-/svg-badge.svg" alt="Translation status" height="20">](https://translate.deepdaikon.xyz/engage/zoysii/)

Zoysii is a simple logic game. You are the red tile on a square board and the aim is to delete almost every tile while trying to make the most points.

It's **so easy**!

___

**Modes:**

* **Single player**: play a random match and try to get the most points.
* **Multiplayer**: play against your opponents and defeat them.
* **Levels**: use your mind to solve each level by deleting all the tiles.

**Main features:**

* Multiplayer mode for up to 4 players on the same device
* 70+ unique levels
* 10+ numeral systems
* Completely free
* No Ads
* Multiple languages
* Minimalist design and dark mode

**Rules:**

The rules may seem difficult at first glance but they are not.

Anyhow, the best way to learn is by playing! Levels mode is a good place to start.

1. You are the red tile on a square board.

2. Swipe horizontally or vertically to move.

3. When you move you reduce tiles value in the direction you are going.

    - The amount of this reduction is equal to your starting point tile value.

    - But if the value of a tile would be equal to 1 or 2, there will be an increase instead of a decrease.

    - Negative numbers become positive.

    - If the value of a tile becomes equal to zero, starting tile value becomes zero too. Tiles have been "Deleted".

4. You earn as many points as the value of the deleted tiles.

5. The aim is to delete almost every tile while trying to make the most points.

6. In multiplayer matches a player can win by deleting opponent's tile.

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/HomePage.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/LevelGame.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/Multiplayer.png" height="320">

## Donate

If you want to support the development of Zoysii you can donate through Liberapay or Ko-fi.

[<img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate" height="30">](https://liberapay.com/deepdaikon/donate)
[<img src="https://cdn.ko-fi.com/cdn/kofi3.png?v=2" alt="Donate" height="30">](https://ko-fi.com/deepdaikon)

## Translate

You can use [Weblate](https://translate.deepdaikon.xyz/engage/zoysii/) if you want to help with the translation of Zoysii.
If your language is missing click on the ["Start new translation"](https://translate.deepdaikon.xyz/projects/zoysii/strings/) button.

[<img src="https://translate.deepdaikon.xyz/widgets/zoysii/-/multi-auto.svg" alt="Translation status">](https://translate.deepdaikon.xyz/engage/zoysii/)

Keep in mind that you need to be signed in Weblate to translate Zoysii. Your username and email address will be placed in the translated files to keep track of the authors of the translations.

You can make anonymous suggestions if you don't want to sign in.

Thank you :)

## What people say

Some of the reviews and comments received:

- "Zoysii is a simple, but fun, logic game that can keep you company during the boring moments of the day!" - [Le Alternative](https://www.lealternative.net/2022/11/11/zoysii/)

- "Best Game in the World" - [A.N.](https://play.google.com/store/apps/details?id=xyz.deepdaikon.z0ysii&reviewId=gp%3AAOqpTOEQwEmf1sBbIazQfvr3R9QWlTNBjdWWyzkShMXMl4uBGq0VgxTU4Z1fLxd6cgL3GT06OlcSw4xFHH8U)

- "A challenging and well designed game that ensures an entertaining mental training. Zoysii allows you to have both linguistic and logic experience at the same time." - [F.C.](https://play.google.com/store/apps/details?id=xyz.deepdaikon.z0ysii&reviewId=gp%3AAOqpTOEhRNf6xVHcCNtCW7muBN64p5ohB6-AvP7qUEfSdAR4XuhVk8n_3v-7a_A0ROHvnK6_73FOSEwesBmp)

- "The game is really cool and challenging and I enjoy thinking and doing the math." - W.V.

- "I really like it! It's simple without being boring, and challenging but still addictive." - [N.P.](https://gitlab.com/deepdaikon/Zoysii/-/issues/2)

- "Yo... so not gonna lie... are there solutions?" - N.
